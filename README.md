# Unofficial TL-R470T+ V6 OpenWrt buildroot

### Note: `main` branch holds reading material and some basic setup for a building attempt. See [approaches](#approaches) for information about other branches.

I do not intend promote this product in any way and the sole purpose of this repository is to provide a modified buildsystem that would allow you to build your own OpenWrt image for the `TL-R470T+ V6` router with extra packages like `openssh-server` which do not come with the official firmware.

Very dissatisfied with this product as a matter of fact. They use free and open source software in their firmware and don't really bother givin' anything back to the community.

# BUILD CURRENTLY FAILING

## Problems
 - It seems like a modified version of the `3.10.14` kernel was used. So since it's missing most of the patches to the kernel fail.
 - Failing to patch and compile nginx.
 - Bricking the router might be irreversable. I found no information on how to unbrick it in case it happens.
 - It is unclear what it takes to make the current firmware accept your image since it doesn't give any useful information after rejecting.

This is gonna be a tough one... But I'll see what I can do...

## What is known?

By doing some reverse engineering with the official firmware image I found out:

 - it uses `U-Boot 1.1.3`
 - `LZMA` for compression
 - the CPU architecture is `MIPS`
 - `Squashfs` root filesystem
 - bootargs:
```
bootargs=console=ttyS0,115200 root=31:02 rootfstype=jffs2 init=/sbin/init mtdparts=raspi:256k(u-boot),64k(u-boot-env),2240k(rootfs),1408k(uImage),64k(mib0),64k(ART)
```
 - `Linux version 3.10.14`
 - `gcc version 4.6.4 (OpenWrt/Linaro GCC 4.6-2013.05 unknown)`
 - Also by opening it up I found out it has `MediaTek MT7628`. This is good, 'cause openwrt seems to support this platform and can produce a `MT7628` image by itself. `TL-R470T+ V6` obviously won't accept this image, but what if we try and mimic image packing process as in its GPL code?
 ![TL-R470T+ V6](https://gitlab.com/Qualphey/tl-r470t-v6-openwrt/-/raw/main/board.jpg "TL-R470T+ V6")

# Approaches

So I've decided to create a branch for each approach. This will help keep everything in order.

## 1. Branch: `A1-hope` -- Manually resolve patch problems and hope for the firmware to function without missing modifications (whatever they are)

## 2. Branch: `A2-mime` -- Build using OpenWrt targeting T7628 platform and try to mimic an official image

---

#If you have any ideas feel free to open up an issue! :)

